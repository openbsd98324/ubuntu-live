# ubuntu-live




````
fdisk  -l mbr-backup-image-sda-mbr-5000.img 

GPT PMBR size mismatch (976773167 != 4999) will be corrected by w(rite).
Disk mbr-backup-image-sda-mbr-5000.img: 2.5 MiB, 2560000 bytes, 5000 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device                             Boot Start       End   Sectors   Size Id Type
mbr-backup-image-sda-mbr-5000.img1          1 976773167 976773167 465.8G ee GPT



fdisk  -l /dev/sda 

Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: 8859D19C-3EF6-46BD-95D0-CDCE6E45AD71

Device       Start       End   Sectors   Size Type
/dev/sda1     2048      4095      2048     1M BIOS boot
/dev/sda2     4096   1054719   1050624   513M EFI System
/dev/sda3  1054720 976771071 975716352 465.3G Linux filesystem
````


````
/dev/sda: PTUUID="8859d19c-3ef6-46bd-95d0-cdce6e45ad71" PTTYPE="gpt"
/dev/sda1: PARTUUID="6d585b98-3863-4a0f-8d4c-a1c3fc4650d1"
/dev/sda2: UUID="367C-5F9C" TYPE="vfat" PARTLABEL="EFI System Partition" PARTUUID="4abbc77a-344a-4b26-9c3e-134788688e35"
/dev/sda3: UUID="d17ad3e2-0b75-4b8a-95ed-96865978f9e9" TYPE="ext4" PARTUUID="551c4eb7-58f9-4225-8a42-5a7f0bf9fbf4"
````





